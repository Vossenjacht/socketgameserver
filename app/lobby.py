import datetime
class Lobby:

    """
    Wat je als client moet versturen:
    b'\x00g{"byteorder": "little", "content-type": "text/json", "content-encoding": "utf-8", "content-length": 37}{"action": "search", "value": "ring"}'

    Wat je als client gaat ontvangen:
    {'result': 'In the caves beneath the Misty Mountains. 💍'}

    """

    def __init__(self):
        # Example {123: -> id assigned by device or by server?
        #               {socket: SOCKET,
        #                position: {x: 5, y: 4},
        #                color: #123123  -> optional color so devices see the same
        #                }}
        self.players = {}

    def onConnect(self, id):
        self.players[id] = True

    def onDisconnect(self, id):
        del self.players[id]

    def update_coordinate(self, id, coordinates):
        #print("LOBBY")
        #print("  Saving coordinates {} for player {}".format(coordinates, id))
        if id not in self.players:
            self.players[id] = {
                "dirty_bit": True,
                "time_last_sync": None,
                "data": {
                    "coordinates": None
                }
            }

        if self.players[id]["data"]["coordinates"] != coordinates:
            self.players[id]["data"]["coordinates"] = coordinates

            # invalidate synced players
            for player_id in self.players:
                self.players[player_id]["dirty_bit"] = True

    def sync_info(self, socketConnection):
        """
        Sync all players with each other
        Only runs when info has changed and enough time has passed
        --
        Geef hier terug wat je naar iedereen wilt versturen.
        """
        if socketConnection.addr in self.players:
            curr_time = datetime.datetime.now()
            player = self.players[socketConnection.addr]
            if player["dirty_bit"] and (player["time_last_sync"] is None or (curr_time - player["time_last_sync"]) > datetime.timedelta(milliseconds=100)):
                socketConnection.add_send_request({
                    "action": "sync",
                    "content": list(map(lambda player: player["data"], self.players.values()))
                })

                player["dirty_bit"] = False
                player["time_last_sync"] = curr_time
