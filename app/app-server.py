#! /bin/python3.7
# BASED ON https://realpython.com/python-sockets/#application-client-and-server
# Moded to a version without headers because only 1 device type will be used probably

import socket
import selectors
import sys
import os
import traceback
import optparse
import time

from libserver import SocketConnection
from lobby import Lobby

# localhost: "127.0.0.1"
HOST = '192.168.2.85'  # Standard loopback interface address (localhost)
PORT = 65431        # Port to listen on (non-privileged ports are > 1023)

def accept_wrapper(sel, sock, lobby):
    conn, addr = sock.accept()  # Should be ready to read
    print("accepted connection from", addr)
    conn.setblocking(False)
    socketConnection = SocketConnection(sel, conn, addr, lobby)
    sel.register(conn, selectors.EVENT_READ, data=socketConnection)

def main():
    """
    Start socket server
    """
    lobby = Lobby()

    sel = selectors.DefaultSelector()

    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Avoid bind() exception: OSError: [Errno 48] Address already in use
    lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    lsock.bind((HOST, PORT))
    lsock.listen()
    print("listening on", (HOST, PORT))
    lsock.setblocking(False)
    sel.register(lsock, selectors.EVENT_READ, data=None)

    start = time.time()

    try:
        while True:

            events = sel.select(timeout=None)
            for key, mask in events:
                if key.data is None:
                    accept_wrapper(sel, key.fileobj, lobby)
                else:
                    socketConnection = key.data
                    try:
                        socketConnection.process_events(mask)
                    except Exception:
                        print(
                            "main: error: exception for",
                            f"{socketConnection.addr}:\n{traceback.format_exc()}",
                        )
                        socketConnection.close()
            else:
                #print("No event")
                pass
    finally:
        sel.close()


if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(), usage=globals()['__doc__'], version='$Id$')
        parser.add_option ('-v', '--verbose', action='store_true', default=False, help='verbose output')
        (options, args) = parser.parse_args()
        #if len(args) < 1:
        #    parser.error ('missing argument')
        if options.verbose: print(time.asctime())
        main()
        if options.verbose: print(time.asctime())
        if options.verbose: print('TOTAL TIME IN MINUTES:',)
        if options.verbose: print((time.time() - start_time) / 60.0)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
